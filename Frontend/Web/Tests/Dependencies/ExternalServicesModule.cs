﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CBD.Frontend.Web.ExternalServices.ExampleModule;
using CBD.Frontend.Web.Test.Mocks.ExternalServices.ExampleModule;
using Ninject.Modules;

namespace CBD.Frontend.Web.Test.Dependencies
{
    class ExternalServicesModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IProductService>().To<ProductServiceMock>().InSingletonScope();
        }
    }
}

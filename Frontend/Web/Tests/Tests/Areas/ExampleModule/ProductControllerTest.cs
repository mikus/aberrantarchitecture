﻿using System;
using System.Linq;
using System.Web.Mvc;
using CBD.Frontend.Web.Areas.ExampleModule.Controllers;
using CBD.Frontend.Web.Areas.ExampleModule.Models;
using CBD.Frontend.Web.ExternalServices.ExampleModule;
using CBD.Frontend.Web.Test.Dependencies;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Ninject.MockingKernel;
using Ninject.MockingKernel.Moq;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace CBD.Frontend.Web.Test.Tests.Areas.ExampleModule
{
    [TestClass]
    public class ProductControllerTest
    {
        private MockingKernel kernel;

        [TestInitialize]
        public void Init()
        {
            kernel = new MoqMockingKernel();
            kernel.Load(new ExternalServicesModule());
            kernel.Bind<ProductController>().ToSelf();
        }

        [TestMethod]
        public void Product_Controller_Should_Return_View_With_Categories()
        {
            // Arrange

            var controller = kernel.Get<ProductController>();

            // Act

            var result = controller.Index() as ViewResult;
            var model = (ProductViewModel)result.Model;

            // Assert

            Assert.That(model, Is.Not.Null);
            Assert.That(model.Products, Is.Empty);
            Assert.That(model.SelectedCategory, Is.Null);
            Assert.That(model.Categories, Has.Length.EqualTo(3));
        }

        [TestMethod]
        public void Product_Controller_Should_Return_View_With_Beverages()
        {
            // Arrange

            var categoryId = 1;
            var productService = kernel.Get<IProductService>();
            var controller = kernel.Get<ProductController>();

            // Act

            var bevCategory = productService.GetCategories()
                                .Where(c => c.Id == categoryId)
                                .SingleOrDefault();
            var result = controller.Index(categoryId) as ViewResult;
            var model = (ProductViewModel)result.Model;

            // Assert

            Assert.That(model, Is.Not.Null);
            Assert.That(model.SelectedCategory, Is.SameAs(bevCategory));
            Assert.That(model.Products, Has.Length.EqualTo(2));
        }        

        [TestCleanup]
        public void Cleanup()
        {
            kernel.Reset();
        }
    }
}

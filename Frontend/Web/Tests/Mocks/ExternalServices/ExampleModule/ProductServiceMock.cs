﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CBD.Frontend.Web.ExternalServices.ExampleModule;

namespace CBD.Frontend.Web.Test.Mocks.ExternalServices.ExampleModule
{
    class ProductServiceMock : IProductService
    {
        // Init categories
        private List<Category> categories = new List<Category>
            {
                new Category { Id = 1, Name = "Beverages" },
                new Category { Id = 2, Name = "Condiments" },
                new Category { Id = 3, Name = "Confections" }
            };

        // Init products
        private Dictionary<int, List<Product>> products = new Dictionary<int, List<Product>>
        {
            { 1, new List<Product>
                    {
                        new Product { Id = 1, Name = "Chai", UnitPrice = 18M },
                        new Product { Id = 2, Name = "Chang", UnitPrice = 19M },
                    }
            },
            { 2, new List<Product>
                    {
                        new Product { Id = 3, Name = "Aniseed Syrup", UnitPrice = 10M }
                    }
            }

        };

        public Category[] GetCategories()
        {
            return categories.ToArray();
        }

        public Product[] GetProducts(int? categoryId)
        {
            if (categoryId == null)
                return new Product[]{};
            else
                return products[(int)categoryId].ToArray();
        }

        // Not Implemented

        public Task<Category[]> GetCategoriesAsync()
        {
            throw new NotImplementedException();
        }

        public Task<Product[]> GetProductsAsync(int? categoryId)
        {
            throw new NotImplementedException();
        }
    }
}

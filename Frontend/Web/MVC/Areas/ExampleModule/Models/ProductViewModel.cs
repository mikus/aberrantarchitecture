﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CBD.Frontend.Web.ExternalServices.ExampleModule;

namespace CBD.Frontend.Web.Areas.ExampleModule.Models
{
    public class ProductViewModel
    {
        public IEnumerable<Category> Categories { get; set; }
        public Category SelectedCategory { get; set; }
        public IEnumerable<Product> Products { get; set; }
    }
}
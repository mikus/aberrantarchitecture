﻿using System.Web.Mvc;

namespace CBD.Frontend.Web.Areas.ExampleModule
{
    public class ExampleModuleAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExampleModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExampleModule_default",
                "ExampleModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}

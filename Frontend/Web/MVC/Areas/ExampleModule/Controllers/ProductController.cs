﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CBD.Frontend.Web.ExternalServices.ExampleModule;
using CBD.Frontend.Web.Areas.ExampleModule.Models;
using CBD.Frontend.Web.Infrastructure.Logging;

namespace CBD.Frontend.Web.Areas.ExampleModule.Controllers
{
    public class ProductController : Controller
    {
        private IProductService productService;
        private ILogger logger;

        public ProductController(IProductService productService, ILogger logger)
        {
            this.productService = productService;
            this.logger = logger;
        }

        //
        // GET: /Product/

        public ActionResult Index()
        {
            // Get view model
            ProductViewModel viewModel = GetProductViewModel(null);

            // Log action
            this.logger.Trace("GET Action: ProductController.Index");
            
            // Return view
            return View(viewModel);
        }

        //
        // POST: /Product/

        [HttpPost]
        public ActionResult Index(int Id)
        {
            // Get view model
            ProductViewModel viewModel = GetProductViewModel(Id);

            // Log action
            this.logger.Trace("POST Action: ProductController.Index");

            // Return view
            return View(viewModel);
        }

        public ProductViewModel GetProductViewModel(int? selectedCategoryId)
        {
            // Get categories
            var categories = this.productService.GetCategories();

            // Get products
            var products = this.productService.GetProducts(selectedCategoryId);

            // Set selected category
            Category selectedCategory = null;
            int categoryId = selectedCategoryId.GetValueOrDefault();
            if (categories.Count() > 0)
            {
                if (categoryId > 0)
                {
                    selectedCategory = (from c in categories
                                        where c.Id == categoryId
                                        select c).FirstOrDefault();
                }
            }

            // Return product view model
            ProductViewModel productViewModel = new ProductViewModel
            {
                Categories = categories,
                SelectedCategory = selectedCategory,
                Products = products
            };
            return productViewModel;
        }
    }
    
}

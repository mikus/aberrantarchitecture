﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using nlog = NLog;

namespace CBD.Frontend.Web.Infrastructure.Logging.NLog
{
    class Logger : ILogger
    {
        private nlog.Logger logger;

        public Logger()
        {
            logger = nlog.LogManager.GetCurrentClassLogger();
        }

        public void Info(string message)
        {
            logger.Info(message);
        }

        public void Warn(string message)
        {
            logger.Warn(message);
        }

        public void Debug(string message)
        {
            logger.Debug(message);
        }

        public void Trace(string message)
        {
            logger.Trace(message);
        }

        public void Error(string message)
        {
            logger.Error(message);
        }

        public void Error(Exception x)
        {
            Error(LogUtility.BuildExceptionMessage(x));
        }

        public void Error(string message, Exception x)
        {
            logger.ErrorException(message, x);
        }

        public void Fatal(string message)
        {
            logger.Fatal(message);
        }

        public void Fatal(Exception x)
        {
            Fatal(LogUtility.BuildExceptionMessage(x));
        }
    }
}
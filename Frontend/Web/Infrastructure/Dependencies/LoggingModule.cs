﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;

using CBD.Frontend.Web.Infrastructure.Logging;
using CBD.Frontend.Web.Infrastructure.Logging.NLog;

namespace CBD.Frontend.Web.Infrastructure.Dependencies
{
    public class LoggingModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ILogger>().To<Logger>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.ServiceModel;
using AutoMapper;
using ExampleModule.Contracts;
using ExampleModule.Data.Access;
using ExampleModule.Core.Facade;
using Dom = ExampleModule.Data.Domain;

namespace ExampleModule.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ProductService : IProductService
    {
        private IProductFacade facade;

        public ProductService() { }

        public ProductService(IProductFacade facade)
        {
            this.facade = facade;
            Mapper.CreateMap<Dom.Category, Category>();
            Mapper.CreateMap<Dom.Product, Product>();
        }
        
        public IEnumerable<Category> GetCategories()
        {
            return Mapper.Map<IEnumerable<Dom.Category>, IEnumerable<Category>>(facade.GetCategories());
        }

        public IEnumerable<Product> GetProducts(int? categoryId)
        {
            return Mapper.Map<IEnumerable<Dom.Product>, IEnumerable<Product>>(facade.GetProducts(categoryId));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using Ninject.Modules;
using ExampleModule.Persistance.Contexts;
using ExampleModule.Persistance.Access;
using ExampleModule.Data.Access;
using System.ServiceModel;

namespace ExampleModule.Infrastructure.Dependencies
{
    public class PersistanceModule : NinjectModule
    {
        public override void Load()
        {
            /*Bind<StoreModelContainer>().ToSelf().InScope(c => OperationContext.Current)
                .WithConstructorArgument("connectionStringOrName", ctx => ctx.Kernel.Get<string>("connectionString"));
            Bind<IStoreUnitOfWork>().To<StoreUnitOfWork>();*/

            Bind<IUnitOfWorkFactory>().To<UnitOfWorkFactory>()
                .WithConstructorArgument("connectionStringOrName", ctx => ctx.Kernel.Get<string>("connectionString"));
        }
    }
}

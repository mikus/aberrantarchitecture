﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Ninject.Modules;
using Ninject.Parameters;
using Ninject.Extensions.Wcf;
using ExampleModule.Services;
using ExampleModule.Contracts;

namespace ExampleModule.Infrastructure.Dependencies
{
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IProductService>().To<ProductService>();
            Bind<ProductService>().ToSelf();
        }
    }
}

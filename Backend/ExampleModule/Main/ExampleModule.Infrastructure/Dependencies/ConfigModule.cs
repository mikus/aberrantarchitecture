﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject.Modules;

namespace ExampleModule.Infrastructure.Dependencies
{
    public class ConfigModule : NinjectModule
    {
        public override void Load()
        {
            var connectionString = "name=StoreModelContainer";
            Bind<string>().ToConstant<string>(connectionString).Named("connectionString");
        }
    }
}

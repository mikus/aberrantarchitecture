﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject.Modules;
using ExampleModule.Core.Facade;
using ExampleModule.Core.Logic;

namespace ExampleModule.Infrastructure.Dependencies
{
    public class CoreModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IProductFacade>().To<ProductFacade>().InSingletonScope();
            Bind<DiscountManager>().ToSelf().InSingletonScope();
        }
    }
}

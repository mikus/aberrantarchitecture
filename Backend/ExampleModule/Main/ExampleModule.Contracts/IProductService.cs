﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ExampleModule.Contracts
{
    [ServiceContract]
    public interface IProductService
    {
        [OperationContract]
        IEnumerable<Category> GetCategories();

        [OperationContract]
        IEnumerable<Product> GetProducts(int? categoryId);
    }
}

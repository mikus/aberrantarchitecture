﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Common.Persistance.Access;
using ExampleModule.Data.Access;
using ExampleModule.Data.Domain;

namespace ExampleModule.Persistance.Access
{
    class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(DbSet<Category> dbset) : base(dbset) { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Common.Persistance.Access;
using ExampleModule.Data.Access;
using ExampleModule.Data.Domain;

namespace ExampleModule.Persistance.Access
{
    class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(DbSet<Product> dbset) : base(dbset) { }

        public IQueryable<Product> FindByName(string name)
        {
            return Find(c => c.Name == name);
        }

        public IQueryable<Product> FindByCategory(Category category)
        {
            return Find(c => c.Category == category);
        }

        public IQueryable<Product> FindByCategoryId(int categoryId)
        {
            return Find(c => c.Category.Id == categoryId);
        }
    }
}

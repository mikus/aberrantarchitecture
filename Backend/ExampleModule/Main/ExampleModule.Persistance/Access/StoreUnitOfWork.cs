﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Persistance.Access;
using ExampleModule.Data.Access;
using ExampleModule.Persistance.Contexts;

namespace ExampleModule.Persistance.Access
{
    public class StoreUnitOfWork : UnitOfWork, IStoreUnitOfWork
    {
        private ICategoryRepository categoryRepository;
        private IProductRepository productRepository;

        private StoreModelContainer context;

        public StoreUnitOfWork(StoreModelContainer context) : base(context) 
        {
            this.context = context;
        }

        public ICategoryRepository CategoryRepository
        {
            get 
            {
                if (categoryRepository == null)
                {
                    categoryRepository = new CategoryRepository(context.CategorySet);
                }
                return categoryRepository;
            }
        }

        public IProductRepository ProductRepository
        {
            get
            {
                if (productRepository == null)
                {
                    productRepository = new ProductRepository(context.ProductSet);
                }
                return productRepository;
            }
        }
    }
}

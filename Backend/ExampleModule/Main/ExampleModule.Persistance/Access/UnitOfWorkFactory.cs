﻿using ExampleModule.Data.Access;
using ExampleModule.Persistance.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExampleModule.Persistance.Access
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private string connectionStringOrName;

        public UnitOfWorkFactory(string connectionStringOrName)
        {
            this.connectionStringOrName = connectionStringOrName;
        }

        public IStoreUnitOfWork createStore()
        {
            return new StoreUnitOfWork(new StoreModelContainer(connectionStringOrName));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using ExampleModule.Data.Domain;

namespace ExampleModule.Persistance.Contexts
{
    public class StoreModelContainer : DbContext
    {
        public StoreModelContainer(string connectionStringOrName) : base(connectionStringOrName)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }

        public DbSet<Category> CategorySet { get; set; }
        public DbSet<Product> ProductSet { get; set; }
    }
}

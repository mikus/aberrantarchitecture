﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExampleModule.Core.Logic
{
    public class DiscountManager
    {
        public virtual decimal calculatePrice(decimal price)
        {
            if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                return Convert.ToDecimal(Math.Round(0.9 * Convert.ToDouble(price), 2));
            return price;
        }
    }
}

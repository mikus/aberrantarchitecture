﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;

using ExampleModule.Data.Access;
using ExampleModule.Data.Domain;
using ExampleModule.Core.Logic;


namespace ExampleModule.Core.Facade
{
    public class ProductFacade : IProductFacade
    {
        private IUnitOfWorkFactory unitOfWorkFactory;
        private DiscountManager discountManager;

        public ProductFacade(IUnitOfWorkFactory unitOfWorkFactory, DiscountManager discountManager)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
            this.discountManager = discountManager;
        }

        public IEnumerable<Category> GetCategories()
        {
            using (var storeUnitOfWork = unitOfWorkFactory.createStore())
            {
                return storeUnitOfWork.CategoryRepository.GetAll();
            }
        }

        public IEnumerable<Product> GetProducts(int? categoryId)
        {
            using (var storeUnitOfWork = unitOfWorkFactory.createStore())
            {
                IEnumerable<Product> products = Enumerable.Empty<Product>();
                if (categoryId != null)
                    products = storeUnitOfWork.ProductRepository.FindByCategoryId((int)categoryId);

                Mapper.CreateMap<Product, Product>();
                products = Mapper.Map<IEnumerable<Product>>(products);
                foreach (var p in products)
                    p.UnitPrice = discountManager.calculatePrice(p.UnitPrice);

                return products;
            }
        }
    }
}

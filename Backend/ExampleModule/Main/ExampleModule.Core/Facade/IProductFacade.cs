﻿using System;
using System.Collections.Generic;
using ExampleModule.Data.Domain;

namespace ExampleModule.Core.Facade
{
    public interface IProductFacade
    {
        IEnumerable<Category> GetCategories();
        IEnumerable<Product> GetProducts(int? categoryId);
    }
}

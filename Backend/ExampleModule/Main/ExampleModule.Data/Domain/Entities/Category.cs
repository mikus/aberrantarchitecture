﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data.Domain;

namespace ExampleModule.Data.Domain
{
    public class Category : IEntity
    {
        public Category()
        {
            this.Product = new HashSet<Product>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}

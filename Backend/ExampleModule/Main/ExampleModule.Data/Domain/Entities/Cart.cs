﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExampleModule.Data.Domain
{
    public class Cart
    {
        private Dictionary<Product, uint> products = new Dictionary<Product,uint>();

        public void Add(Product product, uint count)
        {
            if (products.ContainsKey(product))
                count += products[product];
            products[product] = count;
        }

        public void RemoveAll()
        {
            products.Clear();
        }

        public void RemoveAll(Product product)
        {
            products.Remove(product);
        }

        public void Remove(Product product, uint count)
        {
            if (products.ContainsKey(product))
            {
                products[product] -= count;
                if (products[product] <= 0)
                    products.Remove(product);
            }
        }

        public decimal TotalPrice
        {
            get
            {
                decimal sum = 0;
                foreach (var product in products.Keys)
                {
                    sum += product.UnitPrice * products[product];
                }
                return sum;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data.Domain;

namespace ExampleModule.Data.Domain
{
    public class Product : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal UnitPrice { get; set; }

        public virtual Category Category { get; set; }
    }
}

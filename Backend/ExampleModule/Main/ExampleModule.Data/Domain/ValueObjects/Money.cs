﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExampleModule.Domain.ValueObjects
{
    public class Money
    {
        public readonly Currency Currency;
        public readonly decimal Amount;

        public Money(decimal amount, Currency currency)
        {
            Amount = amount;
            Currency = currency;
        }

        /*public Money AddFunds(Money fundsToAdd)
        {
            // because the money we're adding might
            // be in a different currency, we'll service 
            // locate a money exchange Domain Service.
            var exchange = ServiceLocator.Find<IMoneyExchange>();
            var normalizedMoney = exchange.CurrentValueFor(fundsToAdd, this.Currency);
            var newAmount = this.Amount + normalizedMoney.Amount;
            return new Money(newAmount, this.Currency);
        }*/
    }

    public enum Currency
    {
        USD,
        GBP,
        EUR,
        JPY
    }
}

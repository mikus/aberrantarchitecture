﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data.Access;
using ExampleModule.Data.Domain;

namespace ExampleModule.Data.Access
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}

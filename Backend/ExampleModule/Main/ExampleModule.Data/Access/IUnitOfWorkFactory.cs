﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExampleModule.Data.Access
{
    public interface IUnitOfWorkFactory
    {
        IStoreUnitOfWork createStore();
    }
}

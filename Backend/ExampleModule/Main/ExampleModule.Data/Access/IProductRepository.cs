﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data.Access;
using ExampleModule.Data.Domain;

namespace ExampleModule.Data.Access
{
    public interface IProductRepository : IRepository<Product>
    {
        IQueryable<Product> FindByName(string Name);
        IQueryable<Product> FindByCategory(Category category);
        IQueryable<Product> FindByCategoryId(int categoryId);
    }
}

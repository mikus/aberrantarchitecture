﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data.Access;

namespace ExampleModule.Data.Access
{
    public interface IStoreUnitOfWork : IUnitOfWork
    {
        ICategoryRepository CategoryRepository { get; }
        IProductRepository ProductRepository { get; }
    }
}

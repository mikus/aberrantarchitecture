﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Description;
using Ninject;
using Ninject.Modules;
using Common.Infrastructure.Dependencies;
using ExampleModule.Infrastructure.Dependencies;
using Ninject.Extensions.Wcf;
using ExampleModule.Contracts;

namespace ExampleModule.Services.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel();
            var modules = new List<INinjectModule>
                {
                    new ConfigModule(),
                    new PersistanceModule(),
                    new CoreModule(),
                    new LoggingModule(),
                    new ServiceModule()
                };
            kernel.Load(modules);

            using(var host = kernel.Get<NinjectServiceHost<ProductService>>())
            {
                host.Open();

                Console.WriteLine("The service is ready at {0}", host.BaseAddresses[0]);
                Console.WriteLine("Press <Enter> to stop the service.");
                Console.ReadLine();

                host.Close();
            }
        }
    }
}

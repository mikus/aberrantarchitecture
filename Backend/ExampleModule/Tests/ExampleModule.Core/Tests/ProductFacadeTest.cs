﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Collections.Generic;
using Ninject;
using Ninject.MockingKernel;
using Ninject.MockingKernel.Moq;
using Moq;
using Assert = NUnit.Framework.Assert;
using NUnit.Framework;

using ExampleModule.Core.Facade;
using ExampleModule.Data.Access;
using ExampleModule.Data.Domain;
using ExampleModule.Infrastructure.Dependencies;
using ExampleModule.Core.Logic;
using ExampleModule.Core.Tests.Dependencies;


namespace ExampleModule.Core.Tests
{
    [TestClass]
    public class ProductFacadeTest
    {
        private MockingKernel kernel;

        #region Additional test attributes
        
        [TestInitialize]
        public void Init()
        {
            kernel = new MoqMockingKernel();
            kernel.Load(new DataModule());
            kernel.Bind<ProductFacade>().ToSelf();
        }
        
        [TestCleanup]
        public void Cleanup()
        {
            kernel.Reset();
        }
        
        #endregion


        [TestMethod]
        public void Should_Get_All_Categories()
        {
            // Arange

            var unitOfWork = kernel.Get<IUnitOfWorkFactory>().createStore();
            var givenCategories = unitOfWork.CategoryRepository.GetAll();
            var facade = kernel.Get<ProductFacade>();
            
            // Act

            var obtainedCategories = facade.GetCategories();
            
            //Assert

            Assert.That(obtainedCategories, Is.Not.Null);
            Assert.That(obtainedCategories.Count(), Is.EqualTo(3));
            Assert.That(obtainedCategories, Is.EquivalentTo(givenCategories));
        }

        [TestMethod]
        public void Should_Get_No_Products()
        {
            // Arange

            var facade = kernel.Get<ProductFacade>();

            // Act

            var obtainedCategories = facade.GetProducts(null);

            //Assert

            Assert.That(obtainedCategories, Is.Not.Null);
            Assert.That(obtainedCategories, Is.Empty);
        }

        [TestMethod]
        public void Should_Return_Beverages()
        {
            // Arrange

            var categoryId = 1;
            var unitOfWork = kernel.Get<IUnitOfWorkFactory>().createStore();
            var givenProducts = unitOfWork.ProductRepository.FindByCategoryId(categoryId);
            var facade = kernel.Get<ProductFacade>();

            // Act

            var obtainedProducts = facade.GetProducts(categoryId);

            // Assert

            Assert.That(obtainedProducts, Is.Not.Null);
            Assert.That(obtainedProducts.Count(), Is.EqualTo(2));
            foreach (var product in obtainedProducts)
            {
                Assert.That(givenProducts, Has.Exactly(1).Property("Id").EqualTo(product.Id));
            }
        }

        [TestMethod]
        public void Should_Return_Beverages_With_Discount()
        {
            // Arrange

            var categoryId = 1;
            var unitOfWork = kernel.Get<IUnitOfWorkFactory>().createStore();
            var products = unitOfWork.ProductRepository;
            var dm = new Mock<DiscountManager>();
            dm.Setup(d => d.calculatePrice(It.IsAny<decimal>())).Returns((decimal v) => v / 10);
            kernel.Bind<DiscountManager>().ToConstant<DiscountManager>(dm.Object);
            var facade = kernel.Get<ProductFacade>();

            // Act

            var obtainedProducts = facade.GetProducts(categoryId);

            // Assert

            foreach (var product in obtainedProducts)
            {
                var reference = products.Get(product.Id);
                Assert.That(product.UnitPrice, Is.EqualTo(reference.UnitPrice / 10));
            }
        }
    }
}

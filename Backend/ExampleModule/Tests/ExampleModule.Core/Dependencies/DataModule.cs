﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using Ninject.Modules;
using Moq;

using ExampleModule.Data.Domain;
using ExampleModule.Data.Access;

namespace ExampleModule.Core.Tests.Dependencies
{
    class DataModule : NinjectModule
    {
        public override void Load()
        {
            var categories = new List<Category>
                {
                    new Category { Id = 1, Name = "Beverages" },
                    new Category { Id = 2, Name = "Condiments" },
                    new Category { Id = 3, Name = "Confections" }
                };

            var products = new List<Product>
            {
                new Product { Id = 1, Name = "Chai", UnitPrice = 18M, Category = categories[0] },
                new Product { Id = 2, Name = "Chang", UnitPrice = 19M, Category = categories[0] },
                new Product { Id = 3, Name = "Aniseed Syrup", UnitPrice = 10M, Category = categories[1] }
            };

            Mock<ICategoryRepository> catRepoMock = new Mock<ICategoryRepository>();
            catRepoMock.Setup(r => r.GetAll()).Returns(categories.AsQueryable());

            Mock<IProductRepository> prodRepoMock = new Mock<IProductRepository>();
            prodRepoMock.Setup(r => r.Get(It.IsAny<int>())).Returns((int i) => products.Where(x => x.Id == i).SingleOrDefault());
            prodRepoMock.Setup(r => r.GetAll()).Returns(products.AsQueryable());
            prodRepoMock.Setup(r => r.FindByCategoryId(It.IsAny<int>())).Returns((int i) => products.Where(x => x.Category.Id == i).AsQueryable());
            
            Mock<IStoreUnitOfWork> uowMock = new Mock<IStoreUnitOfWork>();
            uowMock.Setup(u => u.CategoryRepository).Returns(catRepoMock.Object);
            uowMock.Setup(u => u.ProductRepository).Returns(prodRepoMock.Object);
            
            Bind<IStoreUnitOfWork>().ToConstant<IStoreUnitOfWork>(uowMock.Object);

            Mock<IUnitOfWorkFactory> uowFactoryMock = new Mock<IUnitOfWorkFactory>();
            uowFactoryMock.Setup(u => u.createStore()).Returns(uowMock.Object);
            Bind<IUnitOfWorkFactory>().ToConstant<IUnitOfWorkFactory>(uowFactoryMock.Object);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject.Modules;

namespace ExampleModule.Persistance.Dependencies
{
    public class ConfigModule : NinjectModule
    {
        public override void Load()
        {
            var connectionString = "metadata=res://*/Models.StoreModel.csdl|res://*/Models.StoreModel.ssdl|res://*/Models.StoreModel.msl;provider=System.Data.SqlClient;provider connection string=\"data source=(local);initial catalog=ExampleModule;integrated security=True;multipleactiveresultsets=True;App=EntityFramework\"";
            Bind<string>().ToConstant<string>(connectionString).Named("connectionString");
        }
    }
}

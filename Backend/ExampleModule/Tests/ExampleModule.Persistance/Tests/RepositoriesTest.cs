﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = NUnit.Framework.Assert;
using Ninject;
using Ninject.MockingKernel;
using Ninject.MockingKernel.Moq;
using ExampleModule.Infrastructure.Dependencies;
using ExampleModule.Data.Access;
using dep = ExampleModule.Persistance.Dependencies;

namespace ExampleModule.Persistance.Tests
{
    [TestClass]
    public class RepositoriesTest
    {
        private MockingKernel kernel;

        #region Additional test attributes

        [TestInitialize]
        public void Init() 
        {
            kernel = new MoqMockingKernel();
            kernel.Load(new dep.ConfigModule(), new PersistanceModule());
        }
        
        [TestCleanup]
        public void Cleanup() 
        {
            kernel.Reset();
        }
        
        #endregion

        [TestMethod]
        public void Should_Get_All_Categories()
        {
            // Arrange

            var unitOfWork = kernel.Get<IUnitOfWorkFactory>().createStore();
            var categoriesRep = unitOfWork.CategoryRepository;

            // Act

            var categories = categoriesRep.GetAll();

            // Assert

            Assert.That(categories != null);
            Assert.That(categories.Count() > 0);
        }

        [TestMethod]
        public void Should_Get_All_Products()
        {
            // Arrange

            var unitOfWork = kernel.Get<IUnitOfWorkFactory>().createStore();
            var productsRep = unitOfWork.ProductRepository;

            // Act

            var products = productsRep.GetAll();

            // Assert

            Assert.That(products != null);
            Assert.That(products.Count() > 0);
        }

        [TestMethod]
        public void Should_Get_Products_For_Beverages_Category()
        {
            // Arrange

            var unitOfWork = kernel.Get<IUnitOfWorkFactory>().createStore();
            var productsRep = unitOfWork.ProductRepository;

            // Act

            var categoryId = 1;
            var products = productsRep.FindByCategoryId(categoryId);

            // Assert

            Assert.That(products != null);
            Assert.That(products.Count() > 0);
        }
    }
}

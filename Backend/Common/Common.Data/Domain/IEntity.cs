﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.Domain
{
    public interface IEntity
    {
        int Id { get; set; }
    } 
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.Access
{
    public interface IUnitOfWork : IDisposable
    {
        void Save();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Common.Data.Domain;

namespace Common.Data.Access
{
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        TEntity Get(int Id);
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        void Delete(TEntity entity);
        void Add(TEntity entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Linq.Expressions;
using Common.Data.Domain;
using Common.Data.Access;

namespace Common.Persistance.Access
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        private DbSet<TEntity> dbset;

        public Repository(DbSet<TEntity> dbset)
        {
            this.dbset = dbset;
        }
        
        public TEntity Get(int Id)
        {
            return Find(c => c.Id == Id).SingleOrDefault();
        }
        
        public IQueryable<TEntity> GetAll()
        {
            return dbset.AsQueryable();
        }
        
        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {

            return dbset.Where(predicate);
        }
        
        public void Delete(TEntity entity)
        {
            dbset.Remove(entity);
        }

        public void Add(TEntity entity)
        {
            dbset.Add(entity);
        }
    } 
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject.Modules;
using NLog;
using NLog.Config;
using Common.Infrastructure.Logging;

namespace Common.Infrastructure.Dependencies
{
    public class LoggingModule : NinjectModule
    {
        public override void Load()
        {
            ILoggingService logger = GetLoggingService();
            Bind<ILoggingService>().ToConstant(logger);
        }

        private ILoggingService GetLoggingService()
        {
            ILoggingService logger = (ILoggingService)LogManager.GetLogger("Logger", typeof(LoggingService));
            return logger;
        }
    }
}
